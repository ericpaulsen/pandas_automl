# Jupyter Notebook data science example

This repo shows 2 sample Jupyter Notebooks and Python code.  

* generic.ipydb are generic Pandas commands to analyze datasets. e.g., number of rows, columns, column names, distribution of values in a column, read in a csv.

* autogluon_tabular.ipynb uses an OSS autoML framework autoGluon from Amazon, to generate supervised machine learning models.

* The sets folder has publicly available datasets used by the notebooks.


###### GitHub integration to Coder remote development platform

[![Open in Coder](https://demo-2.cdr.dev/static/image/embed-button.svg)](https://demo-2.cdr.dev/workspaces/git?org=602bfeac-2ddfe82d4b0841f8c2cd605c&image=610d89b1-a16ebe06bb3736240cce7496&tag=ubuntu&service=63eaf35f-2abdfb90606d11a5373980cd&repo=git@gitlab.com:ericpaulsen/pandas_automl.git)

###### Notes / To run this app after cloning, we recommend:

* Install anaconda or a Python environment with Jupyter Notebook  https://www.anaconda.com/products/individual

* install autogluon from here: https://autogluon.mxnet.io/index.html#installation
